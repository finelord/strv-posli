# Readme #

Posli - STRV Test Project

### Demo ###

[Posli](http://abaibakou.com/posli)

### Project structure ###

* `app` - source code
* `dist` - linted & assembled & compiled & minified project
* `test` - tests (not implemented)

### How do I get set up? ###

To start development:

```
#!bash
mkdir posli
cd posli
git clone https://finelord@bitbucket.org/finelord/strv-posli.git
npm install
bower install
gulp serve
```

To prepare production `dist`:
```
#!bash
gulp
```