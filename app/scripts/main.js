(function (window, document, smoothScroll) {
    'use strict';

    // helpers

    function hasClass(element, cls) {
        return !!element.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
    }

    function addClass(element, cls) {
        if (!hasClass(element, cls)) {
            element.className += ' ' + cls;
        }
    }

    function removeClass(element, cls) {
        if (hasClass(element, cls)) {
            const reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
            element.className = element.className.replace(reg, '');
        }
    }

    // navbar

    const nav = document.getElementById('nav');
    const navFixed = document.getElementById('navFixed');

    window.onscroll = function () {
        const navOffset = nav.offsetTop + nav.offsetHeight;
        const scrollTop = (window.pageYOffset !== undefined) ?
            window.pageYOffset :
            (document.documentElement ||
             document.body.parentNode ||
             document.body).scrollTop;

        if (scrollTop >= navOffset) {
            addClass(navFixed, 'fixed-top');
        } else {
            removeClass(navFixed, 'fixed-top');
        }
    };

    // validate

    function validateForm (form) {
        const isValid = form.checkValidity();
        let valid = false;
        if (!isValid) {
            addClass(form, 'validation');
        } else {
            valid = true;
        }
        return valid;
    }

    // event handlers

    document.getElementById('headerRequestSubmit').addEventListener('click', () => {
        const form = document.getElementById('headerRequestForm');
        const input = document.getElementById('headerRequestInput');
        const isValid = validateForm(form);
        if (isValid && input.value) {
            const share = document.getElementById('share');
            addClass(form, 'hidden');
            removeClass(share, 'hidden');
        }
    });

    document.getElementById('footerRequestSubmit').addEventListener('click', () => {
        const form = document.getElementById('footerRequestForm');
        const isValid = validateForm(form);
        if (isValid) {
            document.getElementById('footerRequestInput').value = '';
        }
    });

    // smoothScroll

    smoothScroll.init();

}(window, document, window.smoothScroll));
